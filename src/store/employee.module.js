import {
    CREATE_EMPLOYEE,
    DELETE_EMPLOYEE,
    FETCH_EMPLOYEES,
    SET_EMPLOYEES,
    SET_NEW_EMPLOYEE,
    UPDATE_NEW_EMPLOYEE_LIST, UPDATE_SELECTED_EMPLOYEE
} from "./actions.type";
import ApiService from '../common/api.service';
import {GET_EMPLOYEE_LIST, EMPLOYEE} from "../common/apiEndPoints";

const intitialState = {
    employees: [],
    employee: null,
    empId: null,
    isLoading: false
};

export const state = {...intitialState};

const getters = {
    employees(state) {
        return state.employees;
    },
    employee(state) {
        return state.employee;
    },
    getDeletedEmpId(state) {
        return state.empId;
    }
};

export const actions = {
    async [FETCH_EMPLOYEES](context) {
        const { data } = await ApiService.query(GET_EMPLOYEE_LIST);
        context.commit(SET_EMPLOYEES, data);
        return data;
    },
    async [CREATE_EMPLOYEE](context, employeeData) {
        const { data } = await ApiService.post(GET_EMPLOYEE_LIST, employeeData);
        context.commit(SET_NEW_EMPLOYEE, data);
        return data;
    },
    async [UPDATE_SELECTED_EMPLOYEE](context, employeeData) {
        const { data } = await ApiService.put(EMPLOYEE.replace('{id}', employeeData.id), employeeData);
        context.commit(SET_NEW_EMPLOYEE, data.id);
        return data;
    },
    async [DELETE_EMPLOYEE](context, empId) {
        const data = await ApiService.delete(EMPLOYEE.replace('{id}', empId));
        if (data) {
            context.commit(UPDATE_NEW_EMPLOYEE_LIST, empId);
            return empId;
        }
    }
};

export const mutations = {
    [SET_EMPLOYEES](state, employees) {
        state.employees = employees;
    },
    [SET_NEW_EMPLOYEE](state, newEmployee) {
        state.employee = newEmployee
    },
    [UPDATE_NEW_EMPLOYEE_LIST](state, empId) {
        state.empId = empId
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};


