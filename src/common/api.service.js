import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import {API_URL} from "./config";

const createApiUrl = url => API_URL + url;

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL;
    },
    query(resource) {
        return Vue.axios.get(createApiUrl(resource)).catch(error => {
            throw new Error(error);
        });
    },
    post(resource, params) {
        return Vue.axios.post(createApiUrl(resource), params);
    },
    put(resource, params) {
        return Vue.axios.put(createApiUrl(resource), params);
    },
    delete(resource) {
        return Vue.axios.delete(createApiUrl(resource)).catch(error => {
            throw new Error(error);
        });
    }
};

export default ApiService;
