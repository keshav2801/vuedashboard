const changeDateFormat = dateString =>  dateString.split("/").reverse().join("-");

export {changeDateFormat};
